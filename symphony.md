# Download
```bash
wget https://get.symfony.com/cli/installer -O - | bash
```

# Symphony PHP

## Check system requierements
```bash
symfony check:requirements
```

## Creating Symfony Applications
```bash
# Complete web aplication
symfony new my_project_name --full

# Minimal web application
symfony new my_project_name
```

## Running Symfony Applications
```bash
cd my-project/
symfony server:start
```
## Setting up an Existing Symfony Project


```bash
cd projects/
git clone ...

cd my-project/
composer install
```
```bash
php bin/console about
```

## Installing Packages
```bash
composer require logger
```

## Checking Security Vulnerabilities
```bash
symfony check:security
```

## Symfony LTS Versions
```bash
symfony new my_project_name --version=lts

symfony new my_project_name --version=next

symfony new my_project_name --version=4.4
```
